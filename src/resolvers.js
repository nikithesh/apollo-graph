const axios = require("axios");
module.exports = {
    Query: {
     
      user: async (_, args) => {
        try {
          const user = await axios.get(
            `https://api.github.com/users/${args.name}`
          );
          return {
            id: user.data.id,
            login: user.data.login,
            avatar_url: user.data.avatar_url
          };
        } catch (error) {
          throw error;
        }
      }
    }
  };
  